@echo off
echo Gevonden apparaten:
FOR /F "usebackq tokens=3" %%A IN (`REG QUERY HKLM\HARDWARE\DEVICEMAP\SERIALCOMM /f Silabser`) DO (
echo %%A|findstr /C:"COM"
)
echo.
set /p port="Geef het juiste COM poort nummer (zonder "COM"): "
cls
echo geselecteerd: COM%port%
avrdude -Cavrdude.conf -patmega1284p -carduino -P\\.\COM%port% -b115200 -D -Uflash:w:PU03+_v1.1.hex:i
cls
echo Er opent een venster. In de onderste balk moet je aantal dingen typen.
echo.
echo 1. Toets zo snel mogelijk ? en dan ^<enter^>
echo.
echo Wacht totdat "Entered service mode" verschijnt.
echo.
echo 2. Toets p=0000 en dan ^<enter^>
echo 3. Toets d=^<apparaatnummer van 5 cijfers^> en dan ^<enter^>
echo 4. Toets m=^<a voor aspider / n voor newline / l voor newline Luxemburg^> en dan ^<enter^>
echo.
echo 5. Toets s en dan ^<enter^>
echo 6. Controleer of alle info klopt.
echo 7. Toets r en dan ^<enter^>
echo Wacht tot het apparaat online komt en test of hij goed werkt.
(
echo [Settings]
echo Port=COM%port%
echo Baud=9600
echo DataBits=8
echo StopBits=1
echo Parity=0
echo Handshake=0
echo [Options]
echo LocalEcho=1
echo AppendLine=3
echo AutoComplete=0
echo KeepHistory=0
echo Topmost=1
) >termite.ini
termite.exe